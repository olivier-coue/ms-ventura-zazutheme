# zazu-simple-theme

> A simple theme for [Zazu](https://github.com/tinytacoteam/zazu).
> Inspired by [alfred-simple](https://github.com/sindresorhus/alfred-simple).

[![Build Status](https://travis-ci.org/danielbayerlein/zazu-simple-theme.svg?branch=master)](https://travis-ci.org/danielbayerlein/zazu-simple-theme)
[![Greenkeeper badge](https://badges.greenkeeper.io/danielbayerlein/zazu-simple-theme.svg)](https://greenkeeper.io/)

## Preview

![screenshot](./screenshot.png)

## Install

Add `danielbayerlein/zazu-simple-theme` as value to `theme` inside of your `~/.zazurc.json` file.

```json
{
  "theme": "danielbayerlein/zazu-simple-theme"
}
```

## License

Copyright (c) 2017 Daniel Bayerlein. See [LICENSE](./LICENSE.md) for details.
